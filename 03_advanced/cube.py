# ADVANCED ***************************************************************************
# content = assignment
#
# date    = 2022-01-07
# email   = contact@alexanderrichtertd.com
# ************************************************************************************

"""
CUBE CLASS

1. CREATE an abstract class "Cube" with the functions:
   translate(x, y, z), rotate(x, y, z), scale(x, y, z) and color(R, G, B)
   All functions store and print out the data in the cube (translate, rotate, scale and color).

2. ADD an __init__(name) and create 3 cube objects.

3. ADD the function print_status() which prints all the variables nicely formatted.

4. ADD the function update_transform(ttype, value).
   "ttype" can be "translate", "rotate" and "scale" while "value" is a list of 3 floats.
   This function should trigger either the translate, rotate or scale function.

   BONUS: Can you do it without using ifs?

5. CREATE a parent class "Object" which has a name, translate, rotate and scale.
   Use Object as the parent for your Cube class.
   Update the Cube class to not repeat the content of Object.
"""


class Object:

    translate = [0, 0, 0]
    rotate    = [0, 0, 0]
    scale     = [0, 0, 0]

    def __init__(self, name):
        self.name = name


class Cube(Object):

    def translate(self, x, y, z):
        self.translate = [x, y, z]
        print(self.name + '.translate: ' + str(self.translate))
        return self.translate
    
    def rotate(self, x, y, z):
        self.rotate = [x, y, z]
        print(self.name + '.rotate: ' + str(self.rotate))
        return self.rotate

    def scale(self, x, y, z):
        self.scale = [x, y, z]
        print(self.name + '.scale: ' + str(self.scale))
        return self.scale

    def color(self, r, g, b):
        self.color = [r, g, b]
        print(self.name + '.color: ' + str(self.color))
        return self.color

    def print_status(self):
        print(f'Name:      {self.name}')
        print(f'Translate: {self.translate}')
        print(f'Rotate :   {self.rotate}')
        print(f'Scale :    {self.scale}')
        print(f'Color :    {self.color}')

    def update_transform(self, ttype, value):
        # if ttype == 'translate':
        #     self.translate = value[0], value[1], value[2]
        # elif ttype == 'rotate':
        #     self.rotate = value[0], value[1], value[2]
        # elif ttype == 'scale':
        #     self.scale = value[0], value[1], value[2]

        transforms = {'translate' : self.translate,
                      'rotate'    : self.rotate,
                      'scale'     : self.scale}
        
        transforms[ttype](value[0], value[1], value[2])


my_cube1 = Cube('cube010')
my_cube1.color(12, 23, 34)
my_cube1.print_status()
my_cube1.update_transform('translate', [20, 30, 40])
my_cube1.update_transform('rotate', [220, 330, 440])
my_cube1.update_transform('scale', [3, 5, 6])
my_cube1.print_status()