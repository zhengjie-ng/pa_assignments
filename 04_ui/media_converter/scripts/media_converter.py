# ******************************************************************************
# content      = convert image sequences or videos to another video file.
#
# date         = 2023-05-14
#
# how to       = __name__ == "__main__"
#
# license      = MIT
# author       = Zheng Jie Ng <zhengjie.zjie@gmail.com>
# ******************************************************************************

import os
import re
import sys
import glob
import json
import yaml
import getpass
import webbrowser

from Qt import QtWidgets, QtCompat, QtCore

# ******************************************************************************
# VARIABLE
TITLE = os.path.splitext(os.path.basename(__file__))[0]
CURRENT_PATH = os.path.dirname(__file__)
DIR_ROOT     = '\\'.join(os.path.dirname(__file__).split('\\')[:-1])
DIR_PRESET   = DIR_ROOT + '\\preset'
DIR_FFMPEG   = DIR_ROOT + '\\ffmpeg'
DIR_CONFIG   = DIR_ROOT + '\\config'
DIR_SCRIPTS  = DIR_ROOT + '\\scripts'

# ******************************************************************************
# CLASS


class MediaConverter:

    def __init__(self):
        path_ui = ("/").join([os.path.dirname(__file__), TITLE + ".ui"])
        self.wgHeader = QtCompat.loadUi(path_ui)
        self.wgHeader.txtedtCmd.setPlainText("")
        self.process = None

        # SIGNAL
        self.wgHeader.btnSourceBrowse.clicked.connect(self.btnSourceBrowse)
        self.wgHeader.btnDestBrowse.clicked.connect(self.btnDestBrowse)
        self.wgHeader.btnSameSource.clicked.connect(self.btnSameSource)
        self.wgHeader.edtSource.textChanged.connect(self.load_preset_command)
        self.wgHeader.edtDest.textChanged.connect(self.load_preset_command)
        self.wgHeader.combxPreset.currentTextChanged.connect(self.load_preset_command)
        self.wgHeader.combxPreset.currentTextChanged.connect(self.change_destination_format)
        self.wgHeader.cbxOverwrite.stateChanged.connect(self.load_preset_command)
        self.wgHeader.spinxFps.valueChanged.connect(self.load_preset_command)
        self.wgHeader.cbxPresetCmd.stateChanged.connect(self.toggle_cbx_preset_cmd)
        self.wgHeader.cbxCustomCmd.stateChanged.connect(self.toggle_cbx_custom_cmd)
        self.wgHeader.btnDestBrowse.clicked.connect(self.btnDestBrowse)
        self.wgHeader.btnSavePreset.clicked.connect(self.btnSavePreset)
        self.wgHeader.btnCopyPreset.clicked.connect(self.btnCopyPreset)
        self.wgHeader.btnDelPreset.clicked.connect(self.btnDelPreset)
        self.wgHeader.btnRun.clicked.connect(self.btnRun)
        self.wgHeader.btnStop.clicked.connect(self.btnStop)
        self.wgHeader.btnHelp.clicked.connect(self.btnHelp)

        # SETUP
        self.wgHeader.btnSavePreset.setStyleSheet("color: gray;")
        self.wgHeader.btnSavePreset.setEnabled(False)

        # Preset Combo Box
        preset_files = [file for file in os.listdir(DIR_PRESET) if file.endswith('.json')]
        for preset_file in preset_files:
            preset_name = preset_file.replace('.json', '')
            self.wgHeader.combxPreset.addItem(preset_name)

        if 'mp4-medium.json' in preset_files:
            self.wgHeader.combxPreset.setCurrentText('mp4-medium')

        # Preset Command
        self.wgHeader.txtedlPreset.setText(self.generate_command(self.wgHeader.combxPreset.currentText()))

        # Run Button

        self.wgHeader.show()

    # *************************************************************************
    # PRESS
    def btnSourceBrowse(self):
        """Popup browser to select source file, filtering
        only image and video types
        """
        file_dialog = QtWidgets.QFileDialog()

        # Set the dialog options
        file_dialog.setFileMode(QtWidgets.QFileDialog.ExistingFile)
        file_dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen)
        file_dialog.setLabelText(QtWidgets.QFileDialog.Accept, "Select")
        file_dialog.setNameFilters(['Image Files (' + ' '.join(['*{}'.format(x) for x in self.media_formats('image_types')]) + ')',
                                    'Video Files (' + ' '.join(['*{}'.format(x) for x in self.media_formats('video_types')]) + ')',
                                    'All Files (*.*)'])

        # Show the dialog and wait for user input
        if file_dialog.exec_() == QtWidgets.QFileDialog.Accepted:
            # Get the selected file path(s)
            selected_file = file_dialog.selectedFiles()[0]
            file_name = selected_file.split('/')[-1]
            file_name = self.convert_4d(file_name)
            file_path_split = selected_file.split('/')
            file_path = '/'.join(file_path_split[:-1]) + '/'+file_name

            self.wgHeader.edtSource.setText(file_path)

    def btnDestBrowse(self):
        """Popup browser to select the destination
        """
        file_dialog = QtWidgets.QFileDialog()

        file_dialog.setFileMode(QtWidgets.QFileDialog.AnyFile)
        file_dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
        file_dialog.setLabelText(QtWidgets.QFileDialog.Accept, "Save")
        file_dialog.setNameFilters(['Video Files (' + ' '.join(['*{}'.format(x) for x in self.media_formats('video_types')]) + ')'])

        # Show the dialog and wait for user input
        if file_dialog.exec_() == QtWidgets.QFileDialog.Accepted:
            # Get the selected file path(s)
            selected_file = file_dialog.selectedFiles()[0]
            self.wgHeader.edtDest.setText(selected_file)

    def btnSameSource(self):
        """Auto detect source file location and copy it into destination
        """
        source_q = self.wgHeader.edtSource.text()
        source_q = source_q.replace('.%d', '')
        source_q = source_q.split('.')
        source_q = '.'.join(source_q[:-1])
        source_q += '.' + self.read_preset_file(self.wgHeader.combxPreset.currentText())['format']
        self.wgHeader.edtDest.setText(source_q)
        self.load_preset_command()

    def btnCopyPreset(self):
        self.wgHeader.txtedlCustom.setText(self.wgHeader.txtedlPreset.toPlainText())

    def btnSavePreset(self):
        if self.wgHeader.txtedlCustom.toPlainText():
            path_ui = ("/").join([os.path.dirname(__file__), 'custom_name.ui'])
            self.wgCustomName = QtCompat.loadUi(path_ui)
            self.wgCustomName.show()

            self.wgCustomName.lblName.setText(getpass.getuser()+'-')
            self.wgCustomName.edtFormat.textChanged.connect(self.customNameChanged)
            self.wgCustomName.edtDescription.textChanged.connect(self.customNameChanged)
            self.wgCustomName.btnSave.clicked.connect(self.btnSave)

        else:
            warning = QtWidgets.QMessageBox()
            warning.setText("Custom command field cannot be empty.")
            warning.setWindowTitle("Warning")
            warning.setIcon(QtWidgets.QMessageBox.Warning)
            warning.setStyleSheet("background-color: rgb(68, 68, 68);color: rgb(201, 202, 204);")
            warning.exec_()

    def btnSave(self):
        """Save custom command to json and refresh dropdown ui
        """
        name_q = self.wgCustomName.lblName.text()
        format_q = self.wgCustomName.edtFormat.text()
        custom_command_q = self.wgHeader.txtedlCustom.toPlainText()
        custom_command_q = re.sub(r'\s+', ' ', custom_command_q).strip()

        command_split = custom_command_q.split(' ')
        command_file_path = []
        command_seperate = []

        name_replacement = {'ffmpeg.exe': '{ffmpeg}',
                            '-start_number': '{start_number_type}',
                            '-vframes': '{vframes_type}',
                            '-framerate': '{framerate_type}',
                            '-r': '{framerate_type}',
                            '-y': ''}

        if self.wgCustomName.cbxReformat.isChecked():
            for command in command_split:
                for key in name_replacement.keys():
                    if key in command:
                        command = command.replace(command, name_replacement.get(key))

                command_ext = command.split('.')[-1]
                if command_ext in self.media_formats('image_types') or command_ext in self.media_formats('video_types'):
                    command_file_path.append(command)

                command_seperate.append(command)

            if '{start_number_type}' in command_seperate:
                index = command_seperate.index('{start_number_type}') + 1
                command_seperate[index] = '{custom_start_number}'

            if '{vframes_type}' in command_seperate:
                index = command_seperate.index('{vframes_type}') + 1
                command_seperate[index] = '{custom_vframes}'

            if '{framerate_type}' in command_seperate:
                index = command_seperate.index('{framerate_type}') + 1
                command_seperate[index] = '{custom_framerate}'

            command_join = ' '.join(command_seperate)

            try:
                command_join = command_join.replace(command_file_path[0], '{custom_input}')
                command_join = command_join.replace(command_file_path[1], '{overwrite_type} {custom_output}')
            except Exception as error:
                print(str(error))

            if '{overwrite_type} {custom_output}' not in command_join:
                command_join += ' {overwrite_type} {custom_output}'
        else:
            command_join = custom_command_q

        preset_path = DIR_PRESET + '\\' + name_q+'.json'

        preset_path = self.version_up(preset_path)
        name_q = os.path.basename(preset_path).replace('.json', '')

        preset = {"title": name_q,
                  "format": format_q,
                  "command": command_join,
                  "raw": custom_command_q
                  }

        with open(preset_path, 'w') as json_file:
            json.dump(preset, json_file, indent=4)

        option_menu_items = [self.wgHeader.combxPreset.itemText(i) for i in range(self.wgHeader.combxPreset.count())]

        if name_q not in option_menu_items:
            self.wgHeader.combxPreset.addItem(name_q)

        self.wgCustomName.close()

    def btnDelPreset(self):
        """Delete selected preset, default present are protected from deletion
        """
        yaml_path = DIR_CONFIG + "\\protected_preset.yml"
        with open(yaml_path, 'r') as yaml_file:
            protected_preset = yaml.load(yaml_file, Loader=yaml.FullLoader)

        preset_q = self.wgHeader.combxPreset.currentText()
        preset_file = DIR_PRESET + '\\' + preset_q + '.json'

        if preset_q in protected_preset:
            warning = QtWidgets.QMessageBox()
            warning.setText("This preset is default and cannot be deleted")
            warning.setWindowTitle("Information")
            warning.setIcon(QtWidgets.QMessageBox.Information)
            warning.setStyleSheet("background-color: rgb(68, 68, 68);color: rgb(201, 202, 204);")
            warning.exec_()

        else:
            option_menu_items = [self.wgHeader.combxPreset.itemText(i) for i in range(self.wgHeader.combxPreset.count())]

            if self.wgHeader.combxPreset.currentText() in option_menu_items:
                index = self.wgHeader.combxPreset.findText(self.wgHeader.combxPreset.currentText())
                self.wgHeader.combxPreset.removeItem(index)
                os.remove(preset_file)

    def btnRun(self):
        """Execute ffmpeg command as QProcess
        """
        if self.wgHeader.cbxPresetCmd.isChecked():
            command = self.generate_command(self.wgHeader.combxPreset.currentText())
        else:
            command = self.wgHeader.txtedlCustom.toPlainText()

        if command:
            if self.process is None:
                self.wgHeader.pbarProgress.setValue(0)
                self.wgHeader.txtedtCmd.clear()
                self.process = QtCore.QProcess()
                self.process.readyReadStandardError.connect(self.handle_stderr)
                self.process.stateChanged.connect(self.handle_state)
                self.process.finished.connect(lambda exitCode, exitStatus: self.process_finished(exitCode, exitStatus))
                self.process.start(command)

                self.wgHeader.btnStop.setEnabled(True)
                self.wgHeader.btnRun.setEnabled(False)

        else:
            warning = QtWidgets.QMessageBox()
            warning.setText('Custom command field cannot be empty')
            warning.setWindowTitle("Warning")
            warning.setIcon(QtWidgets.QMessageBox.Warning)
            warning.setStyleSheet("background-color: rgb(68, 68, 68);color: rgb(201, 202, 204);")
            warning.exec_()

    def btnStop(self):
        """kill ongoing QProcess
        """
        if self.process is not None and self.process.state() == QtCore.QProcess.Running:
            self.process.kill()
            self.message('Error: Process Stopped by User.')
            self.wgHeader.btnStop.setEnabled(False)
            self.wgHeader.btnRun.setEnabled(True)

    def btnHelp(self):
        webbrowser.open("https://gitlab.com/zhengjie-ng/pa_assignments/-/wikis/home")

    # *************************************************************************
    # FUNCTION

    def media_formats(self, media):
        """Using yml config file for media file type, anyone could change the
        config without piror scripting experience

        Args:
            media (str): Either 'image_types' or 'video_types'

        Returns:
            list: The list of media type formats chosen.
        """

        yaml_path = DIR_CONFIG + "\\media_formats.yml"
        with open(yaml_path, 'r') as yaml_file:
            data = yaml.load(yaml_file, Loader=yaml.FullLoader)

        return data[media]

    def convert_4d(self, file_name):
        """Convert the name of image sequence file, etc image.1001.png
        into image.%d.png

        Args:
            file_name (str): Image sequence file name

        Returns:
            str: Newly converted image sequence file name
        """

        match = re.search(r'\d{4}', file_name)

        if match:
            file_name = re.sub(r'\d{4}', '%d', file_name)

        return file_name

    def version_up(self, file):
        """Detect if there is an existing version number of the file,
        version up if needed

        Args:
            file (str): file name

        Returns:
            str: new file name
        """

        new_file = ''

        if os.path.isfile(file):
            file_path, extension = os.path.splitext(file)
            new_file = file_path + '_1' + extension

            count = 1
            while os.path.isfile(new_file):
                count += 1
                new_file = file_path + '_' + str(count) + extension

        else:
            new_file = file

        return new_file

    def change_destination_format(self):
        if self.wgHeader.edtDest.text():
            destination_format = self.read_preset_file(self.wgHeader.combxPreset.currentText())['format']
            destination_q = self.wgHeader.edtDest.text()
            destination_q = destination_q.split('.')
            destination_q = '.'.join(destination_q[:-1])
            destination_q += '.' + destination_format
            self.wgHeader.edtDest.setText(destination_q)

    def read_preset_file(self, preset_name):
        """Reading of preset json file

        Args:
            preset_name (str): The name of the preset json file

        Returns:
            dict: The data of the json file in dict format
        """

        with open(DIR_PRESET + '\\' + preset_name + '.json', 'r') as preset_data:
            data = json.load(preset_data)

        return data

    def toggle_cbx_preset_cmd(self):
        preset_command_check_q = self.wgHeader.cbxPresetCmd.isChecked()

        if preset_command_check_q:
            self.wgHeader.txtedlPreset.setFixedHeight(160)
            self.wgHeader.btnSavePreset.setFixedHeight(0)
            self.wgHeader.btnCopyPreset.setFixedHeight(0)
            self.wgHeader.cbxCustomCmd.setChecked(False)
            self.wgHeader.txtedlCustom.setEnabled(False)
            self.wgHeader.txtedlCustom.setStyleSheet("color: gray;")
            self.wgHeader.btnSavePreset.setStyleSheet("color: gray;")
            self.wgHeader.btnSavePreset.setEnabled(False)

        else:
            self.wgHeader.txtedlPreset.setFixedHeight(0)
            self.wgHeader.btnSavePreset.setFixedHeight(12)
            self.wgHeader.btnCopyPreset.setFixedHeight(12)
            self.wgHeader.cbxCustomCmd.setChecked(True)
            self.wgHeader.txtedlCustom.setEnabled(True)
            self.wgHeader.txtedlCustom.setStyleSheet("color: white;")
            self.wgHeader.btnSavePreset.setStyleSheet("color: white;")
            self.wgHeader.btnSavePreset.setEnabled(True)

    def toggle_cbx_custom_cmd(self):
        custom_command_check_q = self.wgHeader.cbxCustomCmd.isChecked()

        if custom_command_check_q:
            self.wgHeader.txtedlCustom.setFixedHeight(160)
            self.wgHeader.btnSavePreset.setFixedHeight(12)
            self.wgHeader.cbxPresetCmd.setChecked(False)
            self.wgHeader.txtedlPreset.setEnabled(False)
            self.wgHeader.txtedlPreset.setStyleSheet("color: gray;")

        else:
            self.wgHeader.txtedlCustom.setFixedHeight(0)
            self.wgHeader.btnSavePreset.setFixedHeight(0)
            self.wgHeader.cbxPresetCmd.setChecked(True)
            self.wgHeader.txtedlPreset.setEnabled(True)
            self.wgHeader.txtedlPreset.setStyleSheet("color: white;")

    def btnCfm(self):
        self.wgCompleted.close()

    def btnOpenFolder(self):
        """Shows folder of converted media
        """
        if self.wgHeader.cbxPresetCmd.isChecked():
            command = self.generate_command(self.wgHeader.combxPreset.currentText())
        else:
            command = self.wgHeader.txtedlCustom.toPlainText()

        folder_path = command.split()[-1]
        folder_path_split = folder_path .split('/')
        folder_path = '/'.join(folder_path_split[:-1])
        os.startfile(folder_path)
        self.wgCompleted.close()

    def customNameChanged(self):
        """Naming convention for custom preset, using windows username
        """
        format_q = self.wgCustomName.edtFormat.text()
        description_q = self.wgCustomName.edtDescription.text()

        self.wgCustomName.lblName.setText(getpass.getuser()+'-'+format_q+'-'+description_q)

    def generate_command(self, preset_name):
        """Replaces ui's input with the relative name in preset json file
        and generate usable ffmpeg command

        Args:
            preset_name (str): The name of the preset json file

        Returns:
            str: ffmpeg command
        """

        ffmpeg = DIR_FFMPEG + '\\ffmpeg.exe '
        with open(DIR_PRESET + '\\' + preset_name + '.json', 'r') as preset_data:
            data = json.load(preset_data)
            dest_q = self.wgHeader.edtDest.text()
            fps_q = self.wgHeader.spinxFps.value()

            # Remove trailing whitespace
            source_q = self.wgHeader.edtSource.text()
            source_q = source_q.rstrip()

            if source_q:
                # Getting the file path, name and extension seperately
                source_q_path = source_q.split('/')
                source_q_path = '/'.join(source_q_path[:-1])
                source_q_file = source_q.split('/')[-1]
                source_q_file = source_q_file.split('.')[0]
                source_q_file_ext = source_q.split('.')[-1]

                # If source is an image type, get first frame of the image sequence
                lowest_number = float('inf')
                custom_start_number = ''
                start_number = ''
                vframes = ''
                custom_vframes = ''

                for file in os.listdir(source_q_path):
                    file_ext = file.split('.')[-1]

                    if file_ext in self.media_formats('image_types') and file.startswith(source_q_file) and file.endswith(source_q_file_ext):

                        number_str = file.split('.')[1]
                        number = int(number_str)

                        if number < lowest_number:
                            lowest_number = number

                        custom_start_number = lowest_number
                        start_number = '-start_number'

                        image_files = glob.glob(source_q_path + '/' + source_q_file + '.*.' + file_ext)
                        total_frames = len(image_files)

                        vframes = '-vframes'
                        custom_vframes = total_frames

                if source_q_file_ext in self.media_formats('video_types'):
                    framerate = '-r'

            else:
                custom_start_number = ''
                start_number = ''
                vframes = ''
                custom_vframes = ''

            overwrite_q = self.wgHeader.cbxOverwrite.isChecked()

            if overwrite_q is True:
                overwrite = '-y'
            else:
                overwrite = ''
                dest_q = self.version_up(dest_q)
            framerate = '-framerate'

            data_replacement = {'{ffmpeg}': ffmpeg,
                                '{custom_input}'       : source_q,
                                '{custom_output}'      : dest_q,
                                '{custom_framerate}'   : str(fps_q),
                                '{custom_start_number}': str(custom_start_number),
                                '{start_number_type}'  : start_number,
                                '{vframes_type}'       : vframes,
                                '{custom_vframes}'     : str(custom_vframes),
                                '{overwrite_type}'     : overwrite,
                                '{framerate_type}'     : framerate
                                }

            for key in data_replacement.keys():
                if key in data['command']:
                    data['command'] = data['command'].replace(key, data_replacement.get(key))

            command = data['command']
            command = re.sub(r"\s+", ' ', command).strip()

        return command

    def load_preset_command(self):
        preset_q = self.wgHeader.combxPreset.currentText()
        preset_command = self.generate_command(preset_q)
        self.wgHeader.txtedlPreset.setText(preset_command)

    def message(self, text):
        """append messages to execution log text field

        Args:
            text (str): status message
        """
        self.wgHeader.txtedtCmd.appendPlainText(text)

    def handle_stderr(self):
        data = self.process.readAllStandardError()
        stderr = bytes(data).decode("utf8")
        # Extract progress if it is in the data.
        progress = self.simple_percent_parser(stderr)
        if progress:
            self.wgHeader.pbarProgress.setValue(progress)
        self.message(stderr)

    def handle_stdout(self):
        data = self.process.readAllStandardOutput()
        stdout = bytes(data).decode("utf8")
        self.message(stdout)

    def handle_state(self, state):
        states = {QtCore.QProcess.NotRunning: 'Not running',
                  QtCore.QProcess.Starting: 'Starting',
                  QtCore.QProcess.Running: 'Running',
                  }
        state_name = states[state]
        self.message(f"State changed: {state_name}")

    def process_finished(self, exitCode, exitStatus):
        if exitStatus == QtCore.QProcess.NormalExit and exitCode == 0:
            self.wgHeader.pbarProgress.setValue(100)
            self.message("Process finished.")
            path_ui = ("/").join([os.path.dirname(__file__), 'completed_dialog.ui'])
            self.wgCompleted = QtCompat.loadUi(path_ui)
            self.wgCompleted.show()
            self.wgCompleted.btnCfm.clicked.connect(self.btnCfm)
            self.wgCompleted.btnOpenFolder.clicked.connect(self.btnOpenFolder)
        else:
            warning = QtWidgets.QMessageBox()
            warning.setText('Error: Please Check Log.')
            warning.setWindowTitle("Warning")
            warning.setIcon(QtWidgets.QMessageBox.Warning)
            warning.setStyleSheet("background-color: rgb(68, 68, 68);color: rgb(201, 202, 204);")
            warning.exec_()

        self.wgHeader.btnStop.setEnabled(False)
        self.wgHeader.btnRun.setEnabled(True)
        self.process = None

    def simple_percent_parser(self, output):
        """
        Matches lines using the progress_re regex,
        returning a single integer for the % progress.
        """
        if self.wgHeader.cbxPresetCmd.isChecked():
            command = self.generate_command(self.wgHeader.combxPreset.currentText())
        else:
            command = self.wgHeader.txtedlCustom.toPlainText()

        command_split = command .split(' ')

        if '-vframes' in command_split:
            index = command_split.index('-vframes') + 1
            total_frame = command_split[index]

        progress_re = re.compile(r"frame=(\d+)")
        output = output.replace(" ", "")
        match = progress_re.search(output)
        if match:
            pc_complete = (int(match.group(1)) / int(total_frame)) * 100
            return int(pc_complete)

# *****************************************************************************
# START UI


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    util = MediaConverter()
    app.exec_()
