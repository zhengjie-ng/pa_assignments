# ******************************************************************************
# content      = maya homepage execution
#
# date         = 2023-05-11
#
# license      = MIT
# author       = Zheng Jie Ng <zhengjie.zjie@gmail.com>
# ******************************************************************************

import os
import sys
import webbrowser

sys.path.append('\\'.join(os.path.dirname(__file__).split('\\')[:-1]))
from extern.Qt import QtWidgets, QtGui, QtCompat


# *********************************************************************
# VARIABLE
TITLE = os.path.splitext(os.path.basename(__file__))[0]
CURRENT_PATH = os.path.dirname(__file__)
IMG_PATH = CURRENT_PATH + "/img/{}.png"


# *********************************************************************
# CLASS
class HomeMaya:

    def __init__(self):
        path_ui = ("/").join([os.path.dirname(__file__), TITLE + ".ui"])
        self.wgHeader = QtCompat.loadUi(path_ui)

        self.preview_img_path = ''
        self.wgHeader.setWindowIcon(QtGui.QPixmap(IMG_PATH.format("maya")))

        # BUTTONS ICONS
        self.wgHeader.lblMaya.setPixmap(QtGui.QPixmap(IMG_PATH.format("maya_with_alpha")))

        self.wgHeader.btnFolder.setIcon(QtGui.QPixmap(IMG_PATH.format("btn_folder")))
        self.wgHeader.btnHelp.setIcon(QtGui.QPixmap(IMG_PATH.format("btn_help")))

        # SIGNAL
        self.wgHeader.btnHelp.clicked.connect(self.press_btnHelp)
        self.wgHeader.btnGoToMaya.clicked.connect(self.press_btnGoToMaya)

        self.wgHeader.btnNew.clicked.connect(self.press_btnNew)
        self.wgHeader.btnOpen.clicked.connect(self.press_btnOpen)

        self.wgHeader.btnFolder.clicked.connect(self.press_btnFolder)
        self.wgHeader.btnInfo.clicked.connect(self.press_btnInfo)

        self.wgHeader.btnRecent.clicked.connect(self.press_btnRecent)
        self.wgHeader.btnGettingStarted.clicked.connect(self.press_btnGettingStarted)
        self.wgHeader.btnLearning.clicked.connect(self.press_btnLearning)
        self.wgHeader.btnWhatsNew.clicked.connect(self.press_btnWhatsNew)
        self.wgHeader.btnCommunity.clicked.connect(self.press_btnCommunity)

        self.wgHeader.cbShowHome.stateChanged.connect(self.stateChange_cbShowHome)

        # SETUP
        self.wgHeader.show()

    # *********************************************************************
    # PRESS
    def press_btnHelp(self):
        webbrowser.open("https://help.autodesk.com/view/MAYAUL/2022/ENU/")

    def press_btnGoToMaya(self):
        print('Launch Maya')

    def press_btnNew(self):
        print('Launch New Scene')

    def press_btnOpen(self):
        print('Open Existing Scene')

    def press_btnFolder(self):
        print('Set Project')

    def press_btnInfo(self):
        print('Show More Information')

    def press_btnRecent(self):
        print('Show Recent Page')

    def press_btnGettingStarted(self):
        print('Show Getting Started Page')

    def press_btnLearning(self):
        print('Show Learning Page')

    def press_btnWhatsNew(self):
        print("Show What's New Page")

    def press_btnCommunity(self):
        print("Show Community Page")

    def stateChange_cbShowHome(self, state):
        if state == 2:
            print('Show Home Screen On Startup')
        else:
            print('Go Straight to Maya on Startup')

# *********************************************************************
# START UI
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    util = HomeMaya()
    app.exec_()