# ******************************************************************************
# content      = convert image sequences or videos to another video file.
#
# date         = 2023-04-19
#
# how to       = need to use it with the media_converter_ui
# dependencies = maya
#
# license      = MIT
# author       = Zheng Jie Ng <zhengjie.zjie@gmail.com>
# ******************************************************************************

import os
import re
import json
import subprocess

import maya.cmds as cmds


def media_formats(media):
    """Specify which image and video types to be used in this tool

    Args:
        media (str): Either 'image_types' or 'video_types'

    Returns:
        list: The list of media type formats chosen.
    """

    media_formats = {'image_types': ['png',
                                     'jpg',
                                     'jpeg',
                                     'exr',
                                     'bmp',
                                     'tif',
                                     'tiff'],
                     'video_types': ['avi',
                                     'mov',
                                     'mp4',
                                     'mkv',
                                     'flv',
                                     'wmv']
                     }
    return media_formats[media]


def get_script_dir():
    """Get current directory of this script

    Returns:
        str: Current script directory
    """

    script_dir = '\\'.join(os.path.dirname(__file__).split('\\')[:-1])
    return script_dir


def load_preset_names():
    """Get list of preset names

    Returns:
        list: List of preset names
    """

    preset_dir = get_script_dir() + '\\preset'
    preset_files = [file for file in os.listdir(preset_dir) if file.endswith('.json')]
    return preset_files


def read_preset_file(preset_name):
    """Reading of the preset json file

    Args:
        preset_name (str): The name of the preset json file

    Returns:
        dict: The data of the json file in dict format
    """

    with open(get_script_dir() + '\\preset\\' + preset_name + '.json', 'r') as preset_data:
        data = json.load(preset_data)

    return data


def generate_command(preset_name):
    """Replaces ui's input with the relative name in preset json file
       and generate usable ffmpeg command

    Args:
        preset_name (str): The name of the preset json file

    Returns:
        str: ffmpeg command
    """

    ffmpeg = get_script_dir() + '\\ffmpeg\\ffmpeg.exe '
    with open(get_script_dir() + '\\preset\\' + preset_name + '.json', 'r') as preset_data:
        data = json.load(preset_data)
        source_q = cmds.textFieldButtonGrp('source', query=True, text=True)
        dest_q = cmds.textFieldButtonGrp('destination', query=True, text=True)
        fps_q = cmds.floatField('fps', query=True, value=True)

        # Getting the file path, name and extension seperately
        source_q_path = source_q.split('/')
        source_q_path = '/'.join(source_q_path[:-1])
        source_q_file = source_q.split('/')[-1]
        source_q_file = source_q_file.split('.')[0]
        source_q_file_ext = source_q.split('.')[-1]

        # If source is an image type, get first frame of the image sequence
        lowest_number = float('inf')
        custom_start_number = ''
        start_number = ''
        try:
            for file in os.listdir(source_q_path):
                file_ext = file.split('.')[-1]

                if file_ext in media_formats('image_types') and file.startswith(source_q_file) and file.endswith(source_q_file_ext):
                    number_str = file.split('.')[1]
                    number = int(number_str)

                    if number < lowest_number:
                        lowest_number = number

                    custom_start_number = lowest_number
                    start_number = '-start_number'
        except Exception as error:
            print(str(error))

        overwrite_q = cmds.checkBox('overwrite', query=True, value=True)

        if overwrite_q is True:
            overwrite = '-y'
            dest_q = version_up(dest_q)
        else:
            overwrite = ''

        framerate = '-framerate'
        if source_q_file_ext in media_formats('video_types'):
            framerate = '-r'

        data_replacement = {'{ffmpeg}': ffmpeg,
                            '{custom_input}': source_q,
                            '{custom_output}': dest_q,
                            '{custom_framerate}': str(fps_q),
                            '{custom_start_number}': str(custom_start_number),
                            '{start_number_type}': start_number,
                            '{overwrite_type}': overwrite,
                            '{framerate_type}': framerate
                            }

        for key in data_replacement.keys():
            if key in data['command']:
                data['command'] = data['command'].replace(key, data_replacement.get(key))

        command = data['command']
        command = re.sub("\s+", ' ', command).strip()

    return command


def version_up(file):
    """Detect if there is an existing version number of the file,
       version up if needed

    Args:
        file (str): file name

    Returns:
        str: new file name
    """

    new_file = ''

    if os.path.isfile(file):
        file_path, extension = os.path.splitext(file)
        new_file = file_path + '_1' + extension

        count = 1
        while os.path.isfile(new_file):
            count += 1
            new_file = file_path + '_' + str(count) + extension

    else:
        new_file = file

    return new_file


def delete_preset():
    """Delete the current preset json file selected in the ui except
       for those that are default for this tool
    """

    protected_preset = ['mp4-medium',
                        'mov-prores422hq',
                        'mp4-medium_stepped']
    preset_q = cmds.optionMenu('preset', query=True, value=True)
    preset_file = get_script_dir() + '\\preset\\' + preset_q + '.json'

    if preset_q not in protected_preset:
        for item in cmds.optionMenu('preset', query=True, itemListLong=True):
            if cmds.menuItem(item, query=True, label=True) == preset_q:
                cmds.deleteUI(item)
                os.remove(preset_file)
    else:
        cmds.confirmDialog(title='Cannot Delete Preset',
                           icon='information',
                           message='This preset is default \nand cannot be deleted.',
                           button=['Okay'])


def save_preset():
    """Save custom command to a json file and refresh dropdown ui
    """

    title_q = cmds.text('custom_preset_name', query=True, label=True)
    format_q = cmds.textFieldGrp('preset_format', query=True, text=True)
    custom_command_q = cmds.scrollField('custom_command', query=True, text=True)
    custom_command_q = re.sub('\s+', ' ', custom_command_q).strip()

    command_split = custom_command_q.split(' ')
    command_file_path = []
    command_seperate = []

    name_replacement = {'ffmpeg.exe': '{ffmpeg}',
                        '-start_number': '{start_number_type}',
                        '-framerate': '{framerate_type}',
                        '-r': '{framerate_type}',
                        '-y': ''}

    if cmds.checkBox('reformat', query=True, value=True):

        for command in command_split:
            for key in name_replacement.keys():
                if key in command:
                    command = command.replace(command, name_replacement.get(key))

            command_ext = command.split('.')[-1]
            if command_ext in media_formats('image_types') or command_ext in media_formats('video_types'):
                command_file_path.append(command)

            command_seperate.append(command)

        if '{start_number_type}' in command_seperate:
            index = command_seperate.index('{start_number_type}') + 1
            command_seperate[index] = '{custom_start_number}'

        if '{framerate_type}' in command_seperate:
            index = command_seperate.index('{framerate_type}') + 1
            command_seperate[index] = '{custom_framerate}'

        command_join = ' '.join(command_seperate)

        try:
            command_join = command_join.replace(command_file_path[0], '{custom_input}')
            command_join = command_join.replace(command_file_path[1], '{overwrite_type} {custom_output}')
        except Exception as error:
            print(str(error))

        if '{overwrite_type} {custom_output}' not in command_join:
            command_join += ' {overwrite_type} {custom_output}'
    else:
        command_join = custom_command_q

    preset_path = get_script_dir() + '\\preset\\' + title_q+'.json'

    preset_path = version_up(preset_path)
    title_q = os.path.basename(preset_path).replace('.json', '')

    preset = {"title": title_q,
              "format": format_q,
              "command": command_join,
              "raw": custom_command_q
              }

    with open(preset_path, 'w') as json_file:
        json.dump(preset, json_file, indent=4)

    option_menu_items = cmds.optionMenu("preset", q=True, itemListLong=True)
    option_menu_label = []
    for item in option_menu_items:
        label = cmds.menuItem(item, q=True, label=True)
        option_menu_label.append(label)

    if title_q not in option_menu_label:
        cmds.menuItem(label=title_q, parent='preset')

    if cmds.window('preset_name', exists=True):
        cmds.deleteUI('preset_name')


def custom_preset_info(*args):
    """Generated a file name of the saved preset, with 'custom-'
       at the beginning of file name
    """
    preset_format_q = cmds.textFieldGrp('preset_format', query=True, text=True)
    preset_quality_q = cmds.textFieldGrp('preset_quality', query=True, text=True)

    cmds.text('custom_preset_name',
              edit=True,
              align='left',
              label='custom-'+preset_format_q+'-'+preset_quality_q)


def convert_4d(file_name):
    """Convert the name of image sequence file, etc image.1001.png
       into image.%d.png to be used in ffmpeg command

    Args:
        file_name (str): Image sequence file name

    Returns:
        str: Newly converted image sequence file name
    """

    match = re.search(r'\d{4}', file_name)

    if match:
        file_name = re.sub(r'\d{4}', '%d', file_name)

    return file_name


def select_source(*args):
    """A browser window to select the source file, with file filter
       of only image and video files
    """
    start_dir = cmds.textFieldButtonGrp('source', query=True, text=True)
    start_dir = start_dir.split('/')
    start_dir = '/'.join(start_dir[:-1])

    fileFilter = 'Image Files ('+' '.join(['*{}'.format(x) for x in media_formats('image_types')]) + ')'
    fileFilter += ';;Video Files ('+' '.join(['*{}'.format(x) for x in media_formats('video_types')]) + ');;All Files (*.*)'
    file_path = cmds.fileDialog2(fileFilter=fileFilter,
                                 caption='Select Source File',
                                 startingDirectory=start_dir,
                                 dialogStyle=2,
                                 fileMode=1,
                                 okCaption='Select',
                                 )
    if file_path:
        file_name = file_path[0].split('/')[-1]
        file_name = convert_4d(file_name)
        file_path_split = file_path[0].split('/')
        file_path = '/'.join(file_path_split[:-1]) + '/'+file_name

        cmds.textFieldButtonGrp('source', edit=True, text=file_path)
        cmds.scrollField('generated_command',
                         edit=True,
                         text=generate_command(cmds.optionMenu('preset', query=True, value=True)))


def select_destination(*args):
    """A browser window to select the destination path of the converted media
    """
    start_dir = cmds.textFieldButtonGrp('source', query=True, text=True)
    start_dir = start_dir.split('/')
    start_dir = '/'.join(start_dir[:-1])
    file_path = cmds.fileDialog2(fileFilter='Video Files ('+' '.join(['*{}'.format(x) for x in media_formats('video_types')]) + ')',
                                 caption='Select Destination',
                                 startingDirectory=start_dir,
                                 dialogStyle=2,
                                 fileMode=0,
                                 okCaption='Select',
                                 )
    if file_path:
        cmds.textFieldButtonGrp('destination', edit=True, text=file_path[0])
        cmds.scrollField('generated_command',
                         edit=True,
                         text=generate_command(cmds.optionMenu('preset', query=True, value=True)))


def toggle_generated_command_checkbox(*args):
    generated_command_check_q = cmds.checkBox('generated_command_check', query=True, value=True)

    if generated_command_check_q:
        cmds.checkBox('custom_command_check', edit=True, value=False)
        cmds.scrollField('custom_command', edit=True, enable=False)
    else:
        cmds.checkBox('custom_command_check', edit=True, value=True)
        cmds.scrollField('custom_command', edit=True, enable=True)


def toggle_custom_command_checkbox(*args):
    custom_command_check_q = cmds.checkBox('custom_command_check', query=True, value=True)

    if custom_command_check_q:
        cmds.checkBox('generated_command_check', edit=True, value=False)
        cmds.scrollField('custom_command', edit=True, enable=True)
    else:
        cmds.checkBox('generated_command_check', edit=True, value=True)
        cmds.scrollField('custom_command', edit=True, enable=False)


def load_preset_command(*args):
    preset_q = cmds.optionMenu('preset', query=True, value=True)
    preset_command = generate_command(preset_q)
    cmds.scrollField('generated_command', edit=True, text=preset_command)


def same_as_source(*args):
    """Auto detect the source file location and copy it into destination path
    """
    source_q = cmds.textFieldButtonGrp('source', query=True, text=True)
    source_q = source_q.replace('.%d', '')
    source_q = source_q.split('.')
    source_q = '.'.join(source_q[:-1])
    source_q += '.' + read_preset_file(cmds.optionMenu('preset', query=True, value=True))['format']
    cmds.textFieldButtonGrp('destination', edit=True, text=source_q)
    cmds.scrollField('generated_command',
                     edit=True,
                     text=generate_command(cmds.optionMenu('preset', query=True, value=True)))


def change_destination_format(*args):
    destination_format = read_preset_file(cmds.optionMenu('preset', query=True, value=True))['format']
    destination_q = cmds.textFieldButtonGrp('destination',
                                            query=True,
                                            text=True)
    destination_q = destination_q.split('.')
    destination_q = '.'.join(destination_q[:-1])
    destination_q += '.' + destination_format
    cmds.textFieldButtonGrp('destination', edit=True, text=destination_q)


def run(command_type, preset_name):
    """Execute the ffmpeg command in the ui window

    Args:
        command_type (boolean): Execute either generated or custom command.
        preset_name (str): Name of the selected preset in ui
    """
    if command_type == 1:
        command = generate_command(preset_name)
    else:
        command = cmds.scrollField('custom_command', query=True, text=True)

    result = subprocess.run(command, stderr=subprocess.PIPE)
    print(result.stderr.decode('utf-8'))

    if result.returncode != 0:
        cmds.confirmDialog(title='Error', message='An error occurred while executing the command.')
    else:
        result = cmds.confirmDialog(title='Command completed',
                                    message='The command has finished executing.',
                                    button=['Confirm', 'Open Folder'])
        if result == 'Open Folder':
            folder_path = command.split()[-1]
            folder_path_split = folder_path .split('/')
            folder_path = '/'.join(folder_path_split[:-1])
            os.startfile(folder_path)
