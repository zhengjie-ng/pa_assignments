# ******************************************************************************
# content      = the user interface for media_converter tool
#
# date         = 2023-04-10
#
# how to       = media_converter_ui()
# dependencies = maya
#
# license      = MIT
# author       = Zheng Jie Ng <zhengjie.zjie@gmail.com>
# ******************************************************************************

import importlib

import maya.cmds as cmds

import scripts.media_converter as media_converter

importlib.reload(media_converter)


def save_preset_name_ui(*args):

    if cmds.window('preset_name', exists=True):
        cmds.deleteUI('preset_name')

    preset_name_window = cmds.window('preset_name',
                                     title='Naming Custom Preset',
                                     width=250,
                                     height=125,
                                     sizeable=False)

    cmds.columnLayout(adjustableColumn=True)
    cmds.separator(height=10)

    cmds.rowLayout(numberOfColumns=3, columnWidth3=(5, 65, 100))
    cmds.text('')
    cmds.text(label='Preset Name:', align='left')
    cmds.text('custom_preset_name', label='', align='left')
    cmds.setParent('..')

    cmds.separator(height=10)

    cmds.rowLayout(numberOfColumns=4, columnWidth4=(5, 65, 100, 100))
    cmds.text('')
    cmds.text(label='Format:')
    cmds.textFieldGrp('preset_format',
                      textChangedCommand=media_converter.custom_preset_info)
    cmds.text(label=' mp4, mov, avi etc')
    cmds.setParent('..')

    cmds.separator(height=10)

    cmds.rowLayout(numberOfColumns=4, columnWidth4=(5, 65, 100, 100))
    cmds.text('')
    cmds.text(label='Quality/ \nDescription: ', align='left')
    cmds.textFieldGrp('preset_quality',
                      textChangedCommand=media_converter.custom_preset_info)
    cmds.text(label=' prores422hq, h264, \nlow, high, etc ')
    cmds.setParent('..')

    cmds.separator(height=10)

    cmds.rowLayout(numberOfColumns=2, columnWidth2=(5, 65))
    cmds.text('')
    cmds.checkBox('reformat',
                  value=True,
                  label='Reformat (recommanded)',
                  annotation='Auto detect parameters such as input, output, framerate etc')
    cmds.setParent('..')

    cmds.separator(height=10)

    cmds.rowLayout(numberOfColumns=2)
    cmds.button('preset_save',
                label='Save',
                width=140,
                height=35,
                command='import scripts.media_converter;scripts.media_converter.save_preset()')
    cmds.button('preset_cancel',
                label='Cancel',
                width=140,
                height=35,
                command='cmds.deleteUI("preset_name")')
    cmds.setParent('..')

    cmds.separator(height=10)

    cmds.setParent('..')

    cmds.showWindow(preset_name_window)


def media_converter_ui():
    ui_title = 'media_converter'

    if cmds.window(ui_title, exists=True):
        print('CLOSE duplicate window')
        cmds.deleteUI(ui_title)

    window = cmds.window(ui_title,
                         title='Media Converter',
                         width=600,
                         height=500,
                         sizeable=False)

    cmds.columnLayout(adjustableColumn=True)

    cmds.separator(height=10)

    cmds.rowLayout(numberOfColumns=4, columnWidth4=(5, 65, 150, 100))
    cmds.text('')
    cmds.text(label='Preset: ')
    cmds.optionMenu('preset',
                    changeCommand=lambda *args: (media_converter.change_destination_format(*args),
                                                 media_converter.load_preset_command(*args)))

    for preset_name in media_converter.load_preset_names():
        preset_name = preset_name.replace('.json', '')
        cmds.menuItem(preset_name, label=preset_name, parent='preset')

    if 'mp4-medium.json' in media_converter.load_preset_names():
        cmds.optionMenu('preset', width=150, edit=True, value='mp4-medium')
    else:
        cmds.optionMenu('preset', width=150, edit=True)

    cmds.button('delete_preset',
                label='Delete Preset',
                height=18,
                command='import scripts.media_converter; scripts.media_converter.delete_preset()')

    cmds.setParent('..')

    cmds.separator(height=10)

    cmds.rowLayout(numberOfColumns=3, columnWidth3=(5, 65, 150))
    cmds.text('')
    cmds.text(label='Source:')
    cmds.textFieldButtonGrp('source',
                            text='',
                            cw=[1, 350], buttonLabel='Browse...',
                            enable=True,
                            annotation='Select Source Images or Video',
                            buttonCommand=media_converter.select_source,
                            textChangedCommand=media_converter.load_preset_command)

    cmds.setParent('..')

    cmds.rowLayout(numberOfColumns=4, columnWidth4=(4, 65, 410, 50))
    cmds.text('')
    cmds.text(label='Destination:')
    cmds.textFieldButtonGrp('destination',
                            text='',
                            cw=[1, 350], buttonLabel='Browse...',
                            enable=True,
                            annotation='Select Destination To Save Video As',
                            buttonCommand=media_converter.select_destination,
                            textChangedCommand=media_converter.load_preset_command)
    cmds.button('same_as_source',
                label='Same As Source',
                command=media_converter.same_as_source)
    cmds.setParent('..')

    cmds.rowLayout(numberOfColumns=2, columnWidth2=(73, 65))
    cmds.text('')
    cmds.checkBox('overwrite',
                  label='Overwrite Existing Destination (If Any)',
                  value=True,
                  changeCommand=media_converter.load_preset_command)
    cmds.setParent('..')

    cmds.separator(height=10)

    cmds.rowLayout(numberOfColumns=3, columnWidth3=(5, 65, 150))
    cmds.text('')
    cmds.text(label='FPS:')
    cmds.floatField('fps',
                    value=25,
                    width=35,
                    showTrailingZeros=False,
                    annotation='Framerate per second',
                    changeCommand=media_converter.load_preset_command)
    cmds.setParent('..')

    cmds.separator(height=10)

    cmds.rowLayout(numberOfColumns=3, columnWidth3=(5, 120, 50))
    cmds.text('')
    cmds.checkBox('generated_command_check',
                  label='Generated Command',
                  value=True,
                  changeCommand=media_converter.toggle_generated_command_checkbox)
    cmds.setParent('..')
    ffmpeg = media_converter.get_script_dir() + '\\ffmpeg\\ffmpeg.exe '

    cmds.scrollField('generated_command',
                     wordWrap=True,
                     numberOfLines=3,
                     text=media_converter.generate_command(cmds.optionMenu('preset',
                                                                           query=True,
                                                                           value=True)),
                     editable=False,
                     height=110)

    cmds.rowLayout(numberOfColumns=3, columnWidth3=(5, 120, 50))
    cmds.text('')
    cmds.checkBox('custom_command_check',
                  label='Custom Command',
                  value=False,
                  changeCommand=media_converter.toggle_custom_command_checkbox)

    cmds.setParent('..')

    cmds.scrollField('custom_command',
                     wordWrap=True,
                     numberOfLines=3,
                     text='',
                     editable=True,
                     enable=False,
                     height=110)

    cmds.separator(height=10)

    cmds.button(label='Save Custom Command As Preset',
                height=40,
                command='media_converter_ui.save_preset_name_ui()')

    cmds.separator(height=10)

    cmds.button(label='Run',
                height=40,
                command='import scripts.media_converter; scripts.media_converter.run(cmds.checkBox("generated_command_check", query=True, value=True), cmds.optionMenu("preset", query=True, value=True))')

    cmds.setParent('..')

    cmds.showWindow(window)
