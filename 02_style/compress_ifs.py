# STYLE **********************************************************************
# content = assignment (Python Advanced)
#
# date    = 2022-01-07
# email   = contact@alexanderrichtertd.com
#*****************************************************************************

import maya.cmds as mc

def set_color(control_names=None, color=None):
    """enable and change the controller shape color.

    Args:
        control_names (list): Defaults to None.
        color (int): Defaults to None.
    """
    
    color_profiles = {1: 4,
                      2: 13,
                      3: 25,
                      4: 17,
                      5: 17,
                      6: 15,
                      7: 6,
                      8: 16}
    
    for control_name in control_names:
        try:
            mc.setAttr(control_name + 'Shape.overrideEnabled', 1)
            mc.setAttr(control_name + 'Shape.overrideColor', color_profiles[color])
        except:
            print("There are errors in the set_color() execution.")

