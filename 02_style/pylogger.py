# STYLE ***************************************************************************
# content = assignment
#
# date    = 2022-01-07
# email   = contact@alexanderrichtertd.com
#************************************************************************************

# original: logging.init.py

def find_caller(self):
    """
    Find the stack frame of the caller so that we can note the source
    file name, line number and function name.
    """
    frame = current_frame()

    # On some versions of IronPython, current_frame() returns None if
    # IronPython isn't run with -X:Frames.

    if frame is not None:
        frame = frame.f_back

    return_value = "(unknown file)", 0, "(unknown function)"

    while hasattr(frame, "f_code"):
        code_object = frame.f_code
        filename = os.path.normcase(code_object.co_filename)

        if filename == _srcfile:
            frame = frame.f_back
            continue

        return_value = (code_object.co_filename, frame.f_lineno, code_object.co_name)
        break

    return return_value

