# #******************************************************************************
# # 02. SHELL
# #******************************************************************************
# 1) Do the following steps only using a shell:

#     a) Create the directory "shell_test"


#     b) Create the file "test_print.py" with a simple print into the directory


#     c) Rename the file to "new_test_print.py"


#     d) List what is in the directory "shell_test" including their file permissions


#     e) Execute the Python file and call the simple print


#     f) Remove the directory "shell_test" with its content


#     BONUS: Solve the tasks without looking them up.




md shell_test
cd shell_test
echo print('test') > test_print.py
ren test_print.py new_test_print.py
python new_test_print.py
cd ..
rmdir /S /Q shell_test


